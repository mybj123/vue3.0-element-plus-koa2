/**
 * 环境配置封装
 */
const env = import.meta.env.MODE || 'prod';
const EnvConfig = {
    dev:{
        baseApi:'/api',
        mockApi:'https://www.fastmock.site/mock/f0cbede5eee49f47388b0a3381ed7467/api'
    },
    test:{
        baseApi:'/',
        mockApi:'https://www.fastmock.site/mock/f0cbede5eee49f47388b0a3381ed7467/api'
    },
    prod:{
        baseApi:'/',
        mockApi:'https://www.fastmock.site/mock/f0cbede5eee49f47388b0a3381ed7467/api'
    }
}
export default {
    env,
    mock:false,
    namespace:'manager',
    ...EnvConfig[env]
}