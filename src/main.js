import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ELIcons from '@element-plus/icons-vue'
import request from './utils/request'
import storage from './utils/storage'
import api from './api'
import store from './store'
import Rocket from './../packages'

// console.log("环境变量=>",import.meta.env)

const app = createApp(App);

for (let iconName in ELIcons) {
	app.component(iconName, ELIcons[iconName])
}
app.directive('has', {
	beforMount: (el, binding) => {
		// 获取按钮权限，注意按钮的key不可以重复，必须唯一
		let actionList = storage.getItem("actionList")
		let values = binding.value;
		// 判断列表中是否有对应的按钮权限表示
		let hasPermission = actionList.includes(values);
		if (!hasPermission) {
			// 隐藏按钮
			el.style = "display:none";
			setTimeout(() => {
				// 删除按钮
				el.parentNode.removeChild(el);
			}, 0)

		}
	}
})
app.config.globalProperties.$request = request;
app.config.globalProperties.$api = api;
app.config.globalProperties.$storage = storage;
app.use(router).use(store).use(ElementPlus, { size: 'small' }).use(Rocket).mount('#app')